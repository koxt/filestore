from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.db import models


class AccountManager(BaseUserManager):
    def create_user(self, email, username, password=None, **kwargs):
        if not email:
            raise ValueError('User must have an email address')
        if not username:
            raise ValueError('User must have an username')
        account = self.model(email=self.normalize_email(email), username=username)
        account.set_password(password)

        account.firstname = kwargs.get('firstname')
        account.lastname = kwargs.get('lastname')
        account.full_clean()
        account.save()
        return account


class Account(AbstractBaseUser):
    email = models.EmailField(unique=True)
    username = models.CharField(max_length=40, unique=True)
    firstname = models.CharField(max_length=40, blank=True, null=True)
    lastname = models.CharField(max_length=40, blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    objects = AccountManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ('username',)

    def __str__(self):
        return self.email

    def get_full_name(self):
        return ' '.join((self.firstname or '', self.lastname or ''))

    def get_short_name(self):
        return self.firstname or ''

    def as_dict(self):
        return dict(
            id=self.id,
            email=self.email,
            username=self.username,
            firstname=self.firstname,
            lastname=self.lastname,
        )
