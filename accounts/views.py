from django.contrib.auth import logout as auth_logout
from django.http import JsonResponse
from django.views import View

from accounts import services


def login(request):
    return services.login(request)


def logout(request):
    auth_logout(request)
    return JsonResponse({'status': 200})


class AccountsView(View):

    def post(self, request):
        return services.create(request.body)
