import json

from django.contrib.auth import authenticate, login as auth_login
from django.core.exceptions import ValidationError
from django.http import JsonResponse

from accounts.models import Account


def as_dict(account_id):
    return Account.objects.get(id=account_id).as_dict()


def create_account(email, username, **kwargs):
    account = Account.objects.create_user(email, username, **kwargs)
    return account


def create(request_body):
    try:
        account = create_account(
            **json.loads(
                request_body.decode('utf-8')
            )
        )
    except ValidationError as e:
        return JsonResponse(status=400, data={'message': '\n'.join(e.messages)})
    except Exception:
        return JsonResponse(status=400, data={'message': 'Bad request'})
    if account is not None:
        return JsonResponse(status=201, data={})
    return JsonResponse(status=400, data=account.as_dict())


def login(request):
    try:
        d = json.loads(request.body.decode('utf-8'))
        account = authenticate(email=d['email'], password=d['password'])
    except Exception:
        return JsonResponse(status=400, data={'message': "Bad request"})
    if account is not None:
        auth_login(request, account)
        return JsonResponse(status=200, data={})
    return JsonResponse(status=401, data={'message': 'Failed login has happened'})
