import json
from django.contrib.auth import authenticate
from django.test import TestCase

from . import services


class AccountLoginTestCase(TestCase):

    def test_correct_login(self):
        email, password = 'a7@a.com', '12345'
        services.create_account(email, 'aa7a', password=password)
        response = self.client.post(
            '/accounts/login/',
            json.dumps({'email': email, 'password': password}),
            content_type="application/json"
        )
        self.assertEqual(response.status_code, 200)

    def test_incorrect_login(self):
        response = self.client.post(
            '/accounts/login/',
            json.dumps({'email': 'a7@a.com', 'password': '12345'}),
            content_type="application/json"
        )
        self.assertEqual(response.status_code, 401)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'message': 'Failed login has happened'}
        )

    def test_bad_request(self):
        response = self.client.post(
            '/accounts/login/',
            None,
            content_type="application/json"
        )
        self.assertEqual(response.status_code, 400)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'message': 'Bad request'}
        )


class LogoutTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.account = services.create_account(email='a@b.com', username='123')
        authenticate(email=cls.account.email, password=cls.account.password)

    def test_logout(self):
        response = self.client.get('/accounts/logout/')
        self.assertEqual(response.status_code, 200)


class CreateTestCase(TestCase):

    def test_validation_error(self):
        response = self.client.post(
            '/accounts/', json.dumps({
                'email': 'aa',
                'username': 'aaaa',
                'firstname': 'aaaa',
                'lastname': 'aaaa',
                'password': '12345',
                'repeat_password': '12345',
            }),
            content_type="application/json"
        )
        self.assertEqual(response.status_code, 400)

    def test_bad_request(self):
        response = self.client.post(
            '/accounts/', 'wrong_value',
            content_type="application/json"
        )
        self.assertEqual(response.status_code, 400)

    def test_create(self):
        response = self.client.post(
            '/accounts/', json.dumps({
                'email': 'aa@bb.com',
                'username': 'aaaa',
                'firstname': 'aaaa',
                'lastname': 'aaaa',
                'password': '12345',
                'repeat_password': '12345',
            }),
            content_type="application/json"
        )
        self.assertEqual(response.status_code, 201)
