import json
import os
import tempfile

from django.contrib.auth import authenticate
from django.contrib.auth.models import AnonymousUser
from django.test import TestCase
from django.test import RequestFactory

from . import services, views
from .services import account_services


def _create_tmp_file(fname, account):
    _, filepath = tempfile.mkstemp(dir=os.path.dirname(__file__))
    return services.create_file_one(
        account,
        filepath,
        fname,
        os.path.getsize(filepath)
    )


class AuthenticatedFilesTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.account = account_services.create_account(email='a@b.com', username='123')
        authenticate(email=cls.account.email, password=cls.account.password)

    def setUp(self):
        self.factory = RequestFactory()

    def test_get(self):
        f = _create_tmp_file('a.txt', self.account)
        request = self.factory.get('/accounts/files/')
        request.user = self.account
        response = views.router_view(request)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            [
                {
                    "id": 1,
                    "name":
                        "a.txt",
                    "owner": {
                        "firstname": None,
                        "lastname": None,
                        "email": "a@b.com",
                        "username": "123",
                        "id": 1
                    }
                }
            ]
        )
        f.delete()

    def test_post_files_per_account(self):
        f = _create_tmp_file('a.txt', self.account)
        request = self.factory.post('/accounts/files/', {'file_to_upload': open(f.upload.name)})
        request.user = self.account
        with self.settings(FILES_PER_ACOUNT=1):
            response = views.router_view(request)
            self.assertEqual(response.status_code, 400)
            self.assertJSONEqual(
                str(response.content, encoding='utf8'),
                {'message': 'Error. Maximum files per account reached'}
            )
        f.delete()

    def test_post(self):
        f, filepath = tempfile.mkstemp(dir=os.path.dirname(__file__))
        request = self.factory.post('/accounts/files/', {'file_to_upload': open(filepath)})
        request.user = self.account
        with self.settings(FILES_PER_ACOUNT=1):
            response = views.router_view(request)
            self.assertEqual(response.status_code, 201)
            account_file = services.get_one(
                    json.loads(str(response.content, encoding='utf8')).get('id')
                )
            self.assertIsNotNone(
                account_file
            )
            account_file.upload.delete()
        os.unlink(filepath)

    def test_delete(self):
        f = _create_tmp_file('a.txt', self.account)
        request = self.factory.delete('/accounts/files/')
        request.user = self.account
        response = views.router_view(request, f.id)
        self.assertEqual(response.status_code, 200)


class AnonymousFilesTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

    def setUp(self):
        self.factory = RequestFactory()

    def test_download(self):
        account = account_services.create_account(email='a@b.com', username='123')
        f = _create_tmp_file('a.txt', account)
        request = self.factory.get('/accounts/files/')
        response = views.get(request, f.id)
        self.assertEqual(response.status_code, 200)
        f.delete()

    def test_post(self):
        request = self.factory.post('/accounts/files/', {'file_to_upload': ''})
        request.user = AnonymousUser()
        response = views.router_view(request)
        self.assertEqual(response.status_code, 403)

    def test_get(self):
        request = self.factory.get('/accounts/files/')
        request.user = AnonymousUser()
        response = views.router_view(request)
        self.assertEqual(response.status_code, 403)

    def test_delete(self):
        request = self.factory.delete('/accounts/files/')
        request.user = AnonymousUser()
        response = views.router_view(request, 1)
        self.assertEqual(response.status_code, 403)
