from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.router_view),
    url(r'^(?P<file_id>\d+)$', views.router_view),
]
