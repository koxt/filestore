import filecmp
import urllib.parse
from django.http import HttpResponse
from django.utils.encoding import iri_to_uri
from django.http import JsonResponse
from django.conf import settings

from . import models
from accounts import services as account_services


def create_file_one(account, upload, name, size):
    return models.AccountFile.objects.create(
        account=account,
        upload=upload,
        name=name,
        size=size,
    )


def get_one(file_id):
    return models.AccountFile.objects.get(id=file_id)


def create(account, ufile):
    if models.AccountFile.objects.filter(account=account).count() >= settings.FILES_PER_ACOUNT:
        return JsonResponse(
            status=400,
            data={'message': "Error. Maximum files per account reached"}
        )
    saved_path = ufile
    orig_file = None
    for sz_file_field in models.AccountFile.objects.filter(size=ufile.size):
        if filecmp.cmp(ufile.temporary_file_path(), sz_file_field.upload.name, shallow=False):
            saved_path = sz_file_field.upload.name
            orig_file = sz_file_field
            break
    new_file_dict = create_file_one(
        account=account,
        upload=saved_path,
        name=ufile.name,
        size=ufile.size,
    ).as_dict()
    if orig_file:
        new_file_dict['orig_file_location'] = orig_file.upload.name
        new_file_dict['orig_file_username'] = orig_file.account.username
    return JsonResponse(
        status=201,
        data=new_file_dict
    )


def download(file_id):
    try:
        dfile = get_one(file_id)
    except models.AccountFile.DoesNotExist:
        return JsonResponse(
            status=400,
            data={'message': 'Error.'}
        )
    response = HttpResponse()

    response["Content-Type"] = 'application/octet-stream'
    response["Content-Disposition"] = "attachment; filename*=utf-8''{0}".format(
        urllib.parse.quote(dfile.name, safe='')
    )

    response['X-Accel-Redirect'] = "{loc}/{uri}".format(
        loc=settings.FILES_NGINX_LOCATION,
        uri=iri_to_uri(dfile.upload.name)
    )
    return response


def get_list(account):
    file_dicts = []
    for fd in models.AccountFile.objects.by_account_range(account.id):
        fd['owner'] = account_services.as_dict(account.id)
        file_dicts.append(fd)
    return JsonResponse(status=200, data=file_dicts, safe=False)


def delete(account, file_id):
    try:
        models.AccountFile.objects.get(id=file_id, account=account).delete()
    except models.AccountFile.DoesNotExist:
        return JsonResponse(
            status=400,
            data={'message': 'Bad request'}
        )
    return JsonResponse(
        status=200,
        data={}
    )
