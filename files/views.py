from filestore.decorators import ajax_login_required
from . import services


def router_view(request, file_id=None):
    if request.method == 'GET':
        if file_id:
            return get(request, file_id)
        else:
            return get_list(request)
    elif request.method == 'POST':
        return post(request)
    elif request.method == 'DELETE':
        return delete(request, file_id)


@ajax_login_required
def get_list(request):
    return services.get_list(request.user)


@ajax_login_required
def post(request):
    return services.create(request.user, request.FILES['file_to_upload'])


@ajax_login_required
def delete(request, file_id):
    return services.delete(request.user, file_id)


def get(request, file_id):
    return services.download(file_id)
