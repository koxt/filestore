import os

from django.conf import settings
from django.db import models
from django.db.models import Q


class FilesManager(models.Manager):

    def by_account_qs(self, account_id):
        return self.filter(account=account_id)

    def by_account_range(self, account_id):
        for f in self.by_account_qs(account_id):
            yield f.as_dict()


class AccountFile(models.Model):

    account = models.ForeignKey('accounts.Account', on_delete=models.CASCADE)
    upload = models.FileField(upload_to=settings.FILES_DIR + '/%Y/%m/%d')
    name = models.CharField(max_length=40)
    size = models.BigIntegerField()

    objects = FilesManager()

    def as_dict(self):
        return dict(
            id=self.id,
            name=self.name,
        )

    def delete(self, *args, **kwargs):
        if not AccountFile.objects.filter(~Q(id=self.id), upload=self.upload).exists():
            os.remove(self.upload.path)
        super().delete(*args, **kwargs)
