
from django.conf.urls import url, include
from filestore import views


urlpatterns = [
    url(r'^$', views.index),

    url(r'^accounts/login_page/$', views.LoginView.as_view(), name='login_page'),
    url(r'^accounts/signup_page/$', views.SignUpView.as_view(), name='signup_page'),
    url(r'^accounts/storage_page/$', views.StorageView.as_view(), name='storage_page'),

    url(r'^accounts/', include('accounts.urls')),

    url(r'^accounts/files/', include('files.urls')),

]
