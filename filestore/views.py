from django.shortcuts import redirect
from django.urls import reverse
from django.views.generic.base import TemplateView


def index(request):
    return redirect(reverse('login_page'))

class LoginView(TemplateView):
    template_name = "accounts/login.html"

class SignUpView(TemplateView):
    template_name = "accounts/signup.html"

class StorageView(TemplateView):
    template_name = "accounts/storage.html"