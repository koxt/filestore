from functools import wraps
from django.http import JsonResponse


def ajax_login_required(view):
    @wraps(view)
    def wrapper(request, *args, **kwargs):
        if not request.user.is_authenticated():
            return JsonResponse(status=403, data={'message': 'Error. Login is required'})
        return view(request, *args, **kwargs)

    return wrapper
